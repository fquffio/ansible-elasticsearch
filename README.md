ElasticSearch
=============

This role's duty is to install [ElasticSearch](https://www.elastic.co/products/elasticsearch).

Variables
---------

### `elasticsearch_version`
The version number of ElasticSearch release to be installed. By default, version **2.2.0** is installed.

### `elasticsearch_checksum`
If set, the downloaded .DEB file is tested against this checksum.

### `elasticsearch_config`
A dictionary of ElasticSearch configuration to be written in `elasticsearch.yml` config file. By default, no configuration is written.

### `elasticsearch_plugins`
A dictionary of ElasticSearch plugins to be installed. For each item in the dictionary, the key represents the plugin name, while the value represents the full string to be passed to `elasticsearch/bin/plugin install` command. By default, no plugin is installed.

Example
-------

```yaml
---
elasticsearch_config:
  action.destructive_requires_name: true
  network.host: "127.0.0.1"
  node.name: Galois

elasticsearch_plugins:
  kopf: lmenezes/elasticsearch-kopf
  head: mobz/elasticsearch-head
```
